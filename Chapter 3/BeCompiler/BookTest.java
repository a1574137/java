package przyk6_str63_BeCompiler;

public class BookTest {
    public static void main(String[] args) {

        //Tworzę tablcię
        Book[] myBooks = new Book[3];

        // Dodaję obiekty
        myBooks[0] = new Book();
        myBooks[1] = new Book();
        myBooks[2] = new Book();

        // Dodaję do obiektów
        myBooks[0].title = "Java rusz głową";
        myBooks[0].author = "Jan Kowalski";
        myBooks[1].title = "Python od podstaw";
        myBooks[1].author = "Nowicki Karol";
        myBooks[2].title = "SQL";
        myBooks[2].author = "Anna Nosowska";

        //Pętla
        int x = 0;

        while (x < myBooks.length) {
            System.out.println(myBooks[x].title + " " + myBooks[x].author);
            x = x + 1;
        }

    }

}
