package przyklad3_str60_tablice;

import java.util.Arrays;

public class Dog {

    public static void main(String[] args) {

        // Tworzę tablicę, która przechowuje obiekty
        Dog[] pets = new Dog[7];

        // Tworzę dwa obiekty
        pets[0] = new Dog();
        pets[1] = new Dog();

        // Tworze trzeci obiekt, który wskazuje na to samo co indeks 0
        pets[3] = pets[0];

        // Drukuję tablicę
        System.out.println(Arrays.toString(pets));
    }

}
