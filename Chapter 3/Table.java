package przyklad2_str59_tablice;

import java.util.Arrays;

public class Table {

    public static void main(String[] args) {

        // Inicjalizacja tablicy
        int [] numberTable = new int[7];

        numberTable[0] = 19;
        numberTable[1] = 22;
        numberTable[2] = 3;

        System.out.println(Arrays.toString(numberTable));

    }

}
