package przyk8_str65_Triangle;

public class Triangle {

    double area;
    int height;
    int length;

    public static void main(String[] args) {

        Triangle[] table = new Triangle[4];

        int x = 0;
        while (x < 4) {

            table[x] = new Triangle();

            table[x].height = (x + 1) * 2;
            table[x].length = x + 4;
            table[x].setArea();

            System.out.println("Triangle: " + x + ", area: " + table[x].area);
            x = x + 1;
        }

        System.out.println(x);  //ile wynosi X

        int y = x;
//        x = 27; // Nie wiem po co ta wartość

        // referencja t5 wskazuje na indeks 2 w tablicy "table"
        Triangle t5 = table[2];
        table[2].area = 343;
        System.out.print("y = " + y);
        System.out.println(", t5 area = " + t5.area);

    }

    public void setArea() {
        area = (height * length) / 2;
    }

}


