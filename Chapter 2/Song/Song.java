package przyklad_4_Song;
// str 42
public class Song {
    String title;
    int duration;

    void play() {
        System.out.println("Playing song: " + title);
    }

    void printDetails() {
        System.out.println("This is: " + title + " time duration: " + duration);
    }
}