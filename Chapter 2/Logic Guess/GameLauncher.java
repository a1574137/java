package LogicGuess;

//str 38
public class GameLauncher {

    public static void main(String[] args) {

        int targetNumber = (int) (Math.random() * 10);
        System.out.println(targetNumber);

        Player p1 = new Player();
        Player p2 = new Player();
        Player p3 = new Player();

        p1.guess = 0;
        p2.guess = 0;
        p3.guess = 0;

        p1.isRight = false;
        p2.isRight = false;
        p3.isRight = false;


        int number1 = p1.guessMethod();
        System.out.println("Player 1 powiedział liczbę: " + number1);


        if (number1 == targetNumber) {
            System.out.println("Koniec gry");
        } else {
            System.out.println("gramy dalej");

        }
    }
}