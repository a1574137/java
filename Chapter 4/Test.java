package PassByValue;

public class Test {
    public static void main(String[] args) {

        int x = 1;
        incrementMethod(x);
        System.out.println(x);
    }

    public static void incrementMethod(int x){
        x++;
        System.out.println(x);
    }

}
