package zad10_str86;

public class StudentTest {

    public static void main(String[] args) {

        Student s1= new Student();
        Student s2 = new Student();
        Student s3 = s1;


        System.out.println("s1 == s2 " + (s1 == s2));  // referencje wkazują na inne obiekty
        System.out.println("s1 == s3 " + (s1 == s3));  // referencje wskazują na ten sam obiekt

    }

}
