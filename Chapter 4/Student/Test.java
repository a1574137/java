package zad11_str87;

public class Test {

    // str 87 Sharpen you pencil

    public static void main(String[] args) {

        // zad 1
        int a = calcArea(7, 12);
        System.out.println("Drukuję wartośc: a = " + a);

        // zad 2
        short c = 7;
        int d = calcArea(c, 15);  // nie ma błędu, gdyż short c --->  mieści się w int
        System.out.println("Drukuję wartość: d = " + d);

        //zad 3
        calcArea(2, 3);

        //zad 4 błąd
        long t = 42;
//        int f = calcArea(t,17); // błąd, gdyż long t ---> nie mieści się w int

        //zad 5 błąd
//        int g = calcArea();

        // zad 6 błąd
//        byte h = calcArea(4,20); // błąd gdyż byte ---> nie mieści się w int
        long p = calcArea(4,20); //           long --->  mieści int

    }

    static int calcArea(int height, int width) {
        return height * width;
    }
}
